// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitPawn.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Grabber.h"
#include "Materials/MaterialInstance.h"
#include "HexTile.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "GameLogicManager.h"
#include "AutoChessGrid.h"


// Sets default values
AUnitPawn::AUnitPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> UnitMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BorderMaterial;
		FConstructorStatics()
			: UnitMesh(TEXT("/Game/UnitMesh.UnitMesh"))
			, BorderMaterial(TEXT("/Game/Materials/BorderMaterial.BorderMaterial"))
			, BlueMaterial(TEXT("/Game/Materials/BlueMaterial.BlueMaterial"))
			, BaseMaterial(TEXT("/Game/Materials/BaseMaterial.BaseMaterial"))
		{
		}
	};

	static FConstructorStatics ConstructorStatics;

	//DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	//RootComponent = DummyRoot;

	UnitMesh = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("UnitMesh"));
	UnitMesh->SetStaticMesh(ConstructorStatics.UnitMesh.Get());
	UnitMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	//UnitMesh->CanEditSimulatePhysics;
	//UnitMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	//UnitMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());
	
	
	//UnitMesh->SetupAttachment(DummyRoot);

}

// Called when the game starts or when spawned
void AUnitPawn::BeginPlay()
{
	
	Super::BeginPlay();
//	AAutoChessGrid* grid;
	UnitMesh->SetSimulatePhysics(true);
	UnitMesh->SetLinearDamping(20.0f);
	UnitMesh->SetAngularDamping(20.0f);
	Tags.Add(TEXT("Unit"));
	TargetUnit = nullptr;
	isActive = false;
	moved = false;
	bCanMove = true;
	bCanAttack = true;
	TurnTimer = 0.5f;
	AD = 5;
	MaxHealth = 100;
	CurrentHealth = MaxHealth;
	AttackSpeed = 1.0f;
}

// Called every frame
void AUnitPawn::Tick(float DeltaTime)
{
	
	Super::Tick(DeltaTime);
	if (CurrentHealth <= 0)
	{
		CurrentHex->isOccupied = false;
		manager->Units.RemoveSingleSwap(this);
		Destroy();
	}

}

void AUnitPawn::ResetMove()
{
	bCanMove = true;
	GetWorldTimerManager().ClearTimer(MoveDelay);
}

void AUnitPawn::ResetAttackTimer()
{
	bCanAttack = true;
	GetWorldTimerManager().ClearTimer(AttackDelay);
}

// Called to bind functionality to input
void AUnitPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AUnitPawn::SetCurrentHex(AHexTile* hex)
{
	if (CurrentPlatform != nullptr)
	{
		
		if (CurrentPlatform->isOccupied)
		{
			CurrentPlatform->isOccupied = false;
		}
		CurrentPlatform = nullptr;
	}
	
	if (CurrentHex != nullptr)
	{
		CurrentHex->isOccupied = false;
	}	
	
	CurrentHex = hex;
	hex->isOccupied = true;
	manager->Units.AddUnique(this);
}

void AUnitPawn::MoveToHex(AHexTile* hex)
{
	SetActorLocation(hex->GetActorLocation() + FVector(0, 0, 30));
	CurrentHex->isOccupied = false;
	CurrentHex = hex;
	hex->isOccupied = true;
	NeighbourHexs = GetNeighbours();
}

void AUnitPawn::SetCurrentPlatform(ASmallPlatform* platform)
{
	if (CurrentPlatform != nullptr)
	{
		CurrentPlatform->isOccupied = false;
	}
	if (CurrentHex != nullptr)
	{
		if (CurrentHex->isOccupied)
		{
			CurrentHex->isOccupied = false;
		}
		CurrentHex = nullptr;
	}
	CurrentPlatform = platform;
	platform->isOccupied = true;
	manager->Units.RemoveSingleSwap(this, true);
}

void AUnitPawn::MoveToPlatform(ASmallPlatform* platform)
{
	SetActorLocation(platform->GetActorLocation() + FVector(0, 0, 30));
	CurrentPlatform->isOccupied = false;
	CurrentPlatform = platform;
	platform->isOccupied = true;
}


void AUnitPawn::MoveToEnemy(AUnitPawn* pawn) // meant to be called inside a tick on a timer
{
	bool bHexInCommon = false;
	bool bNextToEachother = false;
	bool bSpaceToMove = false;
	int min;
	AHexTile* hexToMove = nullptr;
	FVector2D enemyCoords = pawn->CurrentHex->coords;
	FVector2D myCoords = CurrentHex->coords;
	TArray<AHexTile*> MyNeighbourHexs, EnemyNeighbourHexs, AvailableNeighbours;
	MyNeighbourHexs = GetNeighbours();
	EnemyNeighbourHexs = pawn->GetNeighbours();
	
	for (auto& hex : MyNeighbourHexs)
	{
		if (!hex->isOccupied)
		{
			AvailableNeighbours.Add(hex);
		}
		if (hex == pawn->CurrentHex)
		{
			bNextToEachother = true;
			break;
		}
	}

	if (AvailableNeighbours.Num() >=1)
	{
		bSpaceToMove = true;
	}

	if (bNextToEachother)
	{
		if (pawn != nullptr)
		{
			if (bCanAttack)
			{
				bCanAttack = false;
				Attack(this, pawn);
				GetWorldTimerManager().SetTimer(AttackDelay, this, &AUnitPawn::ResetAttackTimer, 1 / AttackSpeed, false);
			}
		
		}
		else return;
		
	}
	else
	{
		if (bSpaceToMove)
		{
			min = 100;
			for (auto & availableHex : AvailableNeighbours)
			{
				int distance = abs(availableHex->coords.X - enemyCoords.X) + abs(availableHex->coords.Y - enemyCoords.Y);
				if (distance < min)
				{
					min = distance;
					hexToMove = availableHex;
				}
			}
			if (bCanMove)
			{
				bCanMove = false;
				MoveToHex(hexToMove);
				GetWorld()->GetTimerManager().SetTimer(MoveDelay, this, &AUnitPawn::ResetMove, TurnTimer, false);
			}
			
		}
		else return;
		
	}

	

	///While this unit is not within *range* units of target hex 'pawn', choose one direction and move in that direction once, t
	
	return;
}

TArray<AHexTile*> AUnitPawn::GetNeighbours()
{
	TArray<AHexTile*> Neighbours;

	AHexTile* myHex = CurrentHex;
	AHexTile* Up;
	AHexTile* Down;
	AHexTile* RU;
	AHexTile* RD;
	AHexTile* LU;
	AHexTile* LD;
	FVector2D myCoords = myHex->coords;
	int X = myCoords.X;
	int Y = myCoords.Y;
	if (myCoords.Y > 0)
	{
		Up = *MyGrid->map.Find(myCoords + FVector2D(0.0f, -1.0f));
	}
	else Up = nullptr;

	if (myCoords.Y < 6)
	{
		Down = *MyGrid->map.Find(myCoords + FVector2D(0.0f, 1.0f));
	}
	else Down = nullptr;

	if (X % 2)
	{
		if (X < 6 && Y < 6)
		{
			RD = *MyGrid->map.Find(myCoords + FVector2D(1.0f, 1.0f));
		}
		else RD = nullptr;
		if (X < 6)
		{
			RU = *MyGrid->map.Find(myCoords + FVector2D(1.0f, 0.0f));
		}
		else RU = nullptr;

		if (X > 0)
		{
			LU = *MyGrid->map.Find(myCoords + FVector2D(-1.0f, 0.0f));
		}
		else LU = nullptr;
		if (X > 0 && Y < 6)
		{
			LD = *MyGrid->map.Find(myCoords + FVector2D(-1.0f, 1.0f));
		}
		else LD = nullptr;
	}
	else
	{
		if (X < 6 && Y > 0)
		{
			RU = *MyGrid->map.Find(myCoords + FVector2D(1.0f, -1.0f));
		}
		else RU = nullptr;
		if (X < 6)
		{
			RD = *MyGrid->map.Find(myCoords + FVector2D(1.0f, 0.0f));
		}
		else RD = nullptr;
		if (X > 0 && Y > 0)
		{
			LU = *MyGrid->map.Find(myCoords + FVector2D(-1.0f, -1.0f));
		}
		else LU = nullptr;
		if (X > 0)
		{
			LD = *MyGrid->map.Find(myCoords + FVector2D(-1.0f, 0.0f));
		}
		else LD = nullptr;
	}

	if (Up != nullptr)
	{
		Neighbours.AddUnique(Up);
	}
	if (Down != nullptr)
	{
		Neighbours.AddUnique(Down);
	}
	if (RU != nullptr)
	{
		Neighbours.AddUnique(RU);
	}
	if (RD != nullptr)
	{
		Neighbours.AddUnique(RD);
	}
	if (LU != nullptr)
	{
		Neighbours.AddUnique(LU);
	}
	if (LD != nullptr)
	{
		Neighbours.AddUnique(LD);
	}

	
	return Neighbours;
}


void AUnitPawn::MoveUp()
{
	AHexTile* hex;
	FVector2D Offset = FVector2D(0.0f, -1.0f);
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	if (myCoords.Y > 0)
	{
		destination = myCoords + Offset;
	}
	else
	{
		destination = myCoords;
	}
	
	hex = *MyGrid->map.Find(destination);
	if (hex != nullptr)
	{
		MoveToHex(hex);
	}
	else return;
	
	
}

void AUnitPawn::MoveDown()
{
	AHexTile* hex;
	FVector2D Offset = FVector2D(0.0f, 1.0f);
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	if (myCoords.Y < 6)
	{
		destination = myCoords + Offset;
	}
	else
	{
		destination = myCoords;
	}
	
	hex = *MyGrid->map.Find(destination);
	if (hex != nullptr)
	{
		MoveToHex(hex);
	}
	else return;
}

void AUnitPawn::MoveRightUp()
{
	AHexTile* hex;
	FVector2D Offset;
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	int x = CurrentHex->coords.X;
	if (x % 2)
	{
		Offset = FVector2D(1.0f, 0.0f);
		if (myCoords.X < 6)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	else
	{
		Offset = FVector2D(1.0f, -1.0f);
		if (myCoords.X < 6 && myCoords.Y > 0)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}

	hex = *MyGrid->map.Find(destination);
	MoveToHex(hex);
}

void AUnitPawn::MoveRightDown()
{
	AHexTile* hex;
	FVector2D Offset;
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	int x = CurrentHex->coords.X;
	if (x % 2)
	{
		Offset = FVector2D(1.0f, 1.0f);
		if (myCoords.X < 6 && myCoords.Y < 6)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	else
	{
		Offset = FVector2D(1.0f, 0.0f);
		if (myCoords.X < 6)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	
	hex = *MyGrid->map.Find(destination);
	MoveToHex(hex);
}

void AUnitPawn::MoveLeftUp()
{
	AHexTile* hex;
	FVector2D Offset;
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	int x = CurrentHex->coords.X;
	if (x % 2)
	{
		Offset = FVector2D(-1.0f, 0.0f);
		if (myCoords.X > 0)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	else
	{
		Offset = FVector2D(-1.0f, -1.0f);
		if (myCoords.X > 0 && myCoords.Y > 0)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	hex = *MyGrid->map.Find(destination);
	MoveToHex(hex);
}

void AUnitPawn::MoveLeftDown()
{
	AHexTile* hex;
	FVector2D Offset;
	FVector2D myCoords;
	FVector2D destination;
	myCoords = CurrentHex->coords;
	int x = myCoords.X;
	if (x % 2)
	{
		Offset = FVector2D(-1.0f, 1.0f);
		if (myCoords.X > 0 && myCoords.Y < 6)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	else
	{
		Offset = FVector2D(-1.0f, 0.0f);
		if (myCoords.X > 0)
		{
			destination = myCoords + Offset;
		}
		else destination = myCoords;
	}
	hex = *MyGrid->map.Find(destination);
	MoveToHex(hex);
}

void AUnitPawn::Attack(AUnitPawn * ThisPawn, AUnitPawn * EnemyPawn)
{
	EnemyPawn->CurrentHealth -= ThisPawn->AD;
}



