// Fill out your copyright notice in the Description page of Project Settings.


#include "HexTile.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"


AHexTile::AHexTile()
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> HexTile;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BorderMaterial;
		FConstructorStatics()
			: HexTile(TEXT("/Game/HexTile_Border.HexTile_Border"))
			, BorderMaterial(TEXT("/Game/Materials/BorderMaterial.BorderMaterial"))
			, BlueMaterial(TEXT("/Game/Materials/BlueMaterial.BlueMaterial"))
			, BaseMaterial(TEXT("/Game/Materials/BaseMaterial.BaseMaterial"))
		{
		}
	};

	static FConstructorStatics ConstructorStatics;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	HexMesh = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("HexMesh"));
	HexMesh->SetStaticMesh(ConstructorStatics.HexTile.Get());
	HexMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	HexMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	HexMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());
	HexMesh->SetupAttachment(DummyRoot);


}

void AHexTile::BeginPlay()
{
	Super::BeginPlay();
	ChangeObjectChannel();
	isOccupied = false;
}

void AHexTile::Hover(bool bOn)
{
	if (bOn)
	{
		HexMesh->SetMaterial(0, BlueMaterial);
	}
	else
	{
		HexMesh->SetMaterial(0, BaseMaterial);
	}
}

void AHexTile::ChangeObjectChannel()
{
	HexMesh->SetCollisionObjectType(ECC_GameTraceChannel1);
}