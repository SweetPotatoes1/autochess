// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "AutoChessPlayerController.h"
#include "GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Components/PrimitiveComponent.h"
#include "UnitPawn.h"
#include "SmallPlatform.h"
#include "HexTile.h"


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("physicshandle"));
	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	wasReleased = false;
	//PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle)
	{
		UE_LOG(LogTemp, Error, TEXT("found component"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing physics handle"), *GetOwner()->GetName());
	}

	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction("Click", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Click", IE_Released, this, &UGrabber::Release);
		//UE_LOG(LogTemp, Error, TEXT("found component"));
	}
	else
	{
		//UE_LOG(LogTemp, Error, TEXT("%s missing input component"), *GetOwner()->GetName());
	}
	
	
}
FHitResult UGrabber::Trace()
{
	FVector loc;
	FVector rot;
	FHitResult hit;
	GetWorld()->GetFirstPlayerController()->DeprojectMousePositionToWorld(loc, rot);


	FVector Start = loc;
    FVector End = Start + (rot * 9000);

	FCollisionQueryParams TraceParams;
	GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_Visibility, TraceParams);
	//DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 100.0f);
	return hit;
}

FHitResult UGrabber::TraceObject()
{
	FVector loc;
	FVector rot;
	FHitResult hit;
	
	GetWorld()->GetFirstPlayerController()->DeprojectMousePositionToWorld(loc, rot);


	FVector Start = loc;
	FVector End = Start + (rot * 9000);

	
	GetWorld()->LineTraceSingleByObjectType(hit, Start, End, (ECC_TO_BITFIELD(ECC_GameTraceChannel1)));

	//DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 100.0f);
	if (hit.GetActor())
	{
	//	UE_LOG(LogTemp, Warning, TEXT("%s"), *hit.GetActor()->GetName());
	}
	return hit;
}

void UGrabber::Grab()
{
	wasReleased = false;
	FHitResult hit = Trace();
	UPrimitiveComponent* ComponentToGrab = hit.GetComponent();
	AActor* ActorHit = hit.GetActor();
	if (ActorHit != nullptr)
	{
		if (!PhysicsHandle)
		{
			return;
		}
		else 
		{
			PhysicsHandle->GrabComponentAtLocation(ComponentToGrab, NAME_None, ComponentToGrab->GetOwner()->GetActorLocation());
		}
		
		//ActorHit->Destroy();
	}


}

void UGrabber::Release()
{
			wasReleased = true;
}




// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PhysicsHandle)
	{
		return;
	}
	hit = TraceObject();

	if (PhysicsHandle->GrabbedComponent)
	{
		
		AActor* HexHit = hit.GetActor();
		if (HexHit != nullptr)
		{
			if (!wasReleased)
			{
				PhysicsHandle->SetTargetLocation(HexHit->GetActorLocation());
			}
			else
			{
				AActor* MyUnit = PhysicsHandle->GrabbedComponent->GetOwner();
				AUnitPawn* mypawn = Cast<AUnitPawn>(MyUnit);
				AHexTile* MyHex = Cast<AHexTile>(HexHit);
				ASmallPlatform* MyPlatform = Cast<ASmallPlatform>(HexHit);
				//if (MyUnit->ActorHasTag(TEXT("Unit")))
				if (mypawn != nullptr && MyHex != nullptr)
				{
					if (!MyHex->isOccupied)
					{
						mypawn->SetCurrentHex(MyHex);
						mypawn->isActive = true;
						mypawn->NeighbourHexs = mypawn->GetNeighbours();
					}
					else
					{
						if (mypawn->CurrentHex != nullptr)
						{
							PhysicsHandle->ReleaseComponent();
							mypawn->SetActorLocation(mypawn->CurrentHex->GetActorLocation() + FVector(0, 0, 30));
						}
						else if (mypawn->CurrentPlatform != nullptr)
						{
							PhysicsHandle->ReleaseComponent();
							mypawn->SetActorLocation(mypawn->CurrentPlatform->GetActorLocation() + FVector(0, 0, 30));
						}
					}
				}
				else if (mypawn != nullptr && MyPlatform != nullptr)
				{
					if (!MyPlatform->isOccupied)
					{
						mypawn->isActive = false;
						mypawn->SetCurrentPlatform(MyPlatform);
					}
					else
						if (mypawn->CurrentPlatform != nullptr)
						{
							PhysicsHandle->ReleaseComponent();
							mypawn->SetActorLocation(mypawn->CurrentPlatform->GetActorLocation() + FVector(0, 0, 30));
						}
						else if (mypawn->CurrentHex != nullptr)
						{
							PhysicsHandle->ReleaseComponent();
							mypawn->SetActorLocation(mypawn->CurrentHex->GetActorLocation() + FVector(0, 0, 30));
						}
				
				}
				else return;
				
				
				//CurrentHex = ActorHit;
				PhysicsHandle->ReleaseComponent();
			
			}
			
		}
		else return;
	

		
		/*
		if (hit.GetActor() != nullptr)
		{
			loc2 = hit.GetActor()->GetActorLocation();
		}
		else return;
		
		*/

		//PhysicsHandle->SetTargetLocation(loc2);
		//UE_LOG(LogTemp, Warning, TEXT("%f %f %f"), loc.X, loc.Y, loc.Z);
	}
	else return;
	
	// ...
}

