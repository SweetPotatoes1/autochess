// Fill out your copyright notice in the Description page of Project Settings.

#include "SmallPlatform.h"
#include "ConstructorHelpers.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ASmallPlatform::ASmallPlatform()
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> SmallPlatform;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		FConstructorStatics()
			: SmallPlatform(TEXT("/Game/SmallPlatform.SmallPlatform"))
			, BaseMaterial(TEXT("/Game/Materials/BaseMaterial.BaseMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	PlatformMesh = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("PlatformMesh"));
	PlatformMesh->SetStaticMesh(ConstructorStatics.SmallPlatform.Get());
	PlatformMesh->SetupAttachment(DummyRoot);
	PlatformMesh->SetMaterial(0, ConstructorStatics.BaseMaterial.Get());
	PlatformMesh->SetRelativeLocation(FVector(0, 0, 0));
	PlatformMesh->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));


}

void ASmallPlatform::BeginPlay()
{
	Super::BeginPlay();
	PlatformMesh->SetCollisionObjectType(ECC_GameTraceChannel1);
	isOccupied = false;
}




