// Fill out your copyright notice in the Description page of Project Settings.


#include "AutoChessPlayerController.h"

AAutoChessPlayerController::AAutoChessPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::GrabHand;
}