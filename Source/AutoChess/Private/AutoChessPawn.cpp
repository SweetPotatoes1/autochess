// Fill out your copyright notice in the Description page of Project Settings.
#include "AutoChessPawn.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "CollisionQueryParams.h"
#include "Engine/World.h"
#include "AutoChessPlayerController.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "DrawDebugHelpers.h"
#include "Grabber.h"


// Sets default values
AAutoChessPawn::AAutoChessPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a first person camera component.
	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	grabber = CreateDefaultSubobject<UGrabber>(TEXT("Grabber"));

	// Position the camera slightly above the eyes.
	//FPSCameraComponent->SetRelativeLocation(FVector(0.0f, 1000.0f, 0.0f));
	// Allow the pawn to control camera rotation.
//	FPSCameraComponent->bUsePawnControlRotation = true;

}


// Called when the game starts or when spawned
void AAutoChessPawn::BeginPlay()
{
	Super::BeginPlay();
	FPSCameraComponent->FieldOfView = 80;
	
}

// Called every frame
void AAutoChessPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	/*if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UCameraComponent* OurCamera = PC->GetViewTarget()->FindComponentByClass<UCameraComponent>())
			{
				FVector Start = OurCamera->GetComponentLocation();
				FVector End = Start + (OurCamera->GetComponentRotation().Vector() * 8000.0f);
				//TraceForBlock(Start, End, true);
			}
		}
		else
		{
			FVector Start, Dir, End;
			PC->DeprojectMousePositionToWorld(Start, Dir);
				End = Start + (Dir * 8000.0f);
			//TraceForBlock(Start, End, false);
		}
	}
	*/
	// Argh
	//FVector loc = GetActorLocation();

	//loc.Z += 100.0f * DeltaTime;

	//SetActorLocation( loc);
}

void AAutoChessPawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);
	OutResult.Rotation = FRotator(-40.0f, -90.0f, 0.0f);
	OutResult.Location = FVector(1340.0f, 1940.0f, 2500.0f);
}

// Called to bind functionality to input
void AAutoChessPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	


}

