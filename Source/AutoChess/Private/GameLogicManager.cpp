// Fill out your copyright notice in the Description page of Project Settings.


#include "GameLogicManager.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Public/WorldCollision.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "HexTile.h"
#include "AutoChessGrid.h"
// Sets default values
AGameLogicManager::AGameLogicManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
}

// Called when the game starts or when spawned
void AGameLogicManager::BeginPlay()
{
	
	Super::BeginPlay();

	
	FCollisionShape MyColSphere = FCollisionShape::MakeSphere(5000.0f);
	TArray <FHitResult> OutHits;
	//DrawDebugSphere(GetWorld(), GetActorLocation(), MyColSphere.GetSphereRadius(), 100, FColor::Purple, true);
	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, GetActorLocation(), GetActorLocation() + FVector(1.0f,1.0f,1.0f), FQuat::Identity, ECC_WorldDynamic, MyColSphere);
	if (isHit)
	{
	
		for (auto& Hit : OutHits)
		{

			AActor* ActorHit = Hit.GetActor();
			if (ActorHit != nullptr)
			{		
			//	Actors.Add(ActorHit);
				AUnitPawn* mypawn = Cast<AUnitPawn>(ActorHit);
				AHexTile* myhex = Cast<AHexTile>(ActorHit);
				if (mypawn != nullptr)
				{
					Units.Add(mypawn);
				}
				if (myhex != nullptr)
				{
					Tiles.Add(myhex);
				}
	
			
			}
		
			
		}
	}
	
	
	
}

// Called every frame
void AGameLogicManager::Tick(float DeltaTime)
{
	
	Super::Tick(DeltaTime);

	for (auto& unit : Units)
	{
		AUnitPawn* mypawn = Cast<AUnitPawn>(unit);
		if (mypawn != nullptr)
		{
			if (mypawn->isActive)
			{
			
			
				mypawn->EnemyUnits = Units;
				mypawn->EnemyUnits.RemoveSingleSwap(mypawn, true);
				for(auto& enemy : mypawn->EnemyUnits)
				{
					if ((mypawn->friendly == enemy->friendly) || (!enemy->isActive)) 
					{
						mypawn->EnemyUnits.RemoveSingleSwap(enemy, true);
					}
				}
				mypawn->TargetUnit = FindClosestEnemy(mypawn->EnemyUnits, mypawn);
					//mypawn->MoveUp();
				
					
				
					
				if (mypawn->TargetUnit != nullptr)
				{

					mypawn->MoveToEnemy(mypawn->TargetUnit);

				}
				else return;
					
					//mypawn->MoveToEnemy(mypawn->TargetUnit);
					//return;
					//mypawn->MoveUp();
				
				
			}
			
			
		}
		else return;
	}
}

AUnitPawn* AGameLogicManager::FindClosestEnemy(TArray<AUnitPawn*> pawns, AUnitPawn* pawn)
{
	FVector2D CurrentLocation = pawn->CurrentHex->coords;
	// CAN RETURN NULL, MAKE SURE TO CHECK FOR NULLS WHEN CALLING 
	//TODO when checking for closest enemy, exclude this pawn from the array of pawns.
	int min = 100;
	int sum;
	AUnitPawn* pawnToReturn = nullptr;
	for (auto& enemy : pawns)
	{
		if (enemy->CurrentHex != nullptr)
		{
			FVector2D EnemyLocation = enemy->CurrentHex->coords;
			FVector2D difference = CurrentLocation - EnemyLocation;
			sum = abs(difference.X) + abs(difference.Y);
			if (sum < min)
			{
				min = sum;
				pawnToReturn = enemy;
			}
		}
		//else return nullptr;
		
		
	}
	return pawnToReturn;
}

