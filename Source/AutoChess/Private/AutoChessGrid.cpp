// Fill out your copyright notice in the Description page of Project Settings.


#include "AutoChessGrid.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "SmallPlatform.h"
#include "DrawDebugHelpers.h"
#include "HexTile.h"
#include "TimerManager.h"
#include "GameLogicManager.h"

// Sets default values
AAutoChessGrid::AAutoChessGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;
	size = 7;
	HexRadiusY = 600.0f;
	HexRadiusX = 520.0f;
	platformX = 2;
	platformY = 6;
	platformSpacing = 400.0f;
	Offset = true;
}

// Called when the game starts or when spawned
void AAutoChessGrid::BeginPlay()
{

	Super::BeginPlay();
	bCanSpawn = true;
	SpawnRate = 4.0f;
	bEnemyCanSpawn = true;
	EnemySpawnRate = 5.0f;
	MyGrid = this;

//	AGameLogicManager logic;

	for (int32 HexIndexX = 0; HexIndexX < size; HexIndexX++)
	{
		//x amount
		for (int32 HexIndexY = 0; HexIndexY < size; HexIndexY++)
		{
			if (Offset)
			{
				xPosition = (HexIndexX * HexRadiusX);
				yPosition = (HexIndexY * HexRadiusY);
			}
			else
			{
				xPosition = (HexIndexX * HexRadiusX);
				yPosition = (HexIndexY * HexRadiusY + 300);
			}


			const FVector NewLocation = (FVector(xPosition, yPosition, 0.0f) + GetActorLocation());
			AHexTile* NewHex = GetWorld()->SpawnActor<AHexTile>(NewLocation, FRotator(0, 0, 0));
			NewHex->coords = FVector2D(HexIndexX, HexIndexY);
			map.Add(NewHex->coords, NewHex);			
		}

		if (Offset)
		{
			Offset = false;
		}
		else
		{
			Offset = true;
		}
	}

	for (int32 platformindexX = 0; platformindexX < platformX; platformindexX++)
	{
		for (int32 platformindexY = 0; platformindexY < platformY; platformindexY++)
		{
			xPosition = (platformindexX * platformSpacing);
			yPosition = (platformindexY * platformSpacing);

			const FVector NewLocation = (FVector(xPosition, yPosition, 0.0f) + GetActorLocation() - FVector(1200.0f, -1200.0f, 0.0f));
			ASmallPlatform* NewPlatform = GetWorld()->SpawnActor<ASmallPlatform>(NewLocation, FRotator(0.0f, 0.0f, 0.0f));
			Platforms.Add(NewPlatform);
		}
	}

}

void AAutoChessGrid::ResetSpawnTimer()
{
	bCanSpawn = true;
	GetWorldTimerManager().ClearTimer(SpawnTimer);
}

void AAutoChessGrid::ResetEnemySpawnTimer()
{
	bEnemyCanSpawn = true;
	GetWorldTimerManager().ClearTimer(EnemySpawnTimer);
}

// Called every frame
void AAutoChessGrid::Tick(float DeltaTime)
{
		for (auto& platform : Platforms)
		{
			ASmallPlatform* MyPlatform = Cast<ASmallPlatform>(platform);
			if (MyPlatform != nullptr)
			{
				if (!MyPlatform->isOccupied)
				{
					if (bCanSpawn)
					{
						MyPlatform->isOccupied = true;
						bCanSpawn = false;
						AUnitPawn* NewUnit = GetWorld()->SpawnActor<AUnitPawn>(UnitBP, MyPlatform->GetActorLocation() + FVector(0, 0, 100), FRotator(0.0f, 0.0f, 0.0f));
					//	if (NewUnit != nullptr)
					//	{
							NewUnit->friendly = false;
							NewUnit->SetCurrentPlatform(MyPlatform);
							GetWorldTimerManager().SetTimer(SpawnTimer, this, &AAutoChessGrid::ResetSpawnTimer, SpawnRate, false);
					//	}
					//	else return;
					
					}
					
				}
				
			}
			
		}

		for (int32 x = 0; x <= 6; x++)
		{
			AHexTile* hex = *map.Find(FVector2D(x, 0));
			if (hex != nullptr)
			{
				if (!hex->isOccupied)
				{
					if (bEnemyCanSpawn)
					{
						bEnemyCanSpawn = false;
						hex->isOccupied = true;
						AUnitPawn* NewUnit = GetWorld()->SpawnActor<AUnitPawn>(UnitBP, hex->GetActorLocation() + FVector(0, 0, 150), FRotator(0.0f, 0.0f, 0.0f));
					//	if (NewUnit != nullptr)
					//	{
							NewUnit->friendly = true;
							NewUnit->isActive = true;
							NewUnit->SetCurrentHex(hex);
							GetWorldTimerManager().SetTimer(EnemySpawnTimer, this, &AAutoChessGrid::ResetEnemySpawnTimer, EnemySpawnRate, false);
					//	}
					//	else return;
					
					}
					
				}
			
			}
		}
}

