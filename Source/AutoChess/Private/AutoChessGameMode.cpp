// Fill out your copyright notice in the Description page of Project Settings.

#include "AutoChessGameMode.h"
#include "AutoChessPlayerController.h"
#include "AutoChessPawn.h"


AAutoChessGameMode::AAutoChessGameMode()
{
	// no pawn by default
	DefaultPawnClass = AAutoChessPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = AAutoChessPlayerController::StaticClass();
}
