// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SmallPlatform.generated.h"


UCLASS()
class AUTOCHESS_API ASmallPlatform : public AActor
{
	GENERATED_BODY()
	UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* PlatformMesh;
public:	
	// Sets default values for this actor's properties
	ASmallPlatform();
	UPROPERTY()
		class UMaterial* BaseMaterial;

protected:


public:	
	class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	class UStaticMeshComponent* GetPlatformMesh() const { return PlatformMesh; }
	
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool isOccupied;

};
