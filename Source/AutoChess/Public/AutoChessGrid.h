// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexTile.h"
#include "SmallPlatform.h"
#include "AutoChessGrid.generated.h"

class AUnitPawn;
UCLASS()
class AUTOCHESS_API AAutoChessGrid : public AActor
{
	GENERATED_BODY()
		UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	
public:	
	// Sets default values for this actor's properties
	AAutoChessGrid();
	int32 size;
	float HexRadiusX;
	float HexRadiusY;
	bool Offset;
	float xPosition;
	float yPosition;
	int32 platformX, platformY;
	float platformSpacing;
	bool bCanSpawn;
	bool bEnemyCanSpawn;
	float SpawnRate;
	float EnemySpawnRate;
	FTimerHandle SpawnTimer;
	FTimerHandle EnemySpawnTimer;
	
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	void ResetSpawnTimer();
	void ResetEnemySpawnTimer();
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<AUnitPawn> UnitBP;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AAutoChessGrid* MyGrid;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FVector2D, AHexTile*> map;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ASmallPlatform*> Platforms;
	virtual void Tick(float DeltaTime) override;
	class USceneComponent* GetDummyRoot() const { return DummyRoot; }
};
