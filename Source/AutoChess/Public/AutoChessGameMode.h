// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AutoChessGameMode.generated.h"

/**
 * 
 */
UCLASS()
class AUTOCHESS_API AAutoChessGameMode : public AGameModeBase
{
	GENERATED_BODY()
		public:
		AAutoChessGameMode();
};
