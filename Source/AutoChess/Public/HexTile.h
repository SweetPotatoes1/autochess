// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexTile.generated.h"

UCLASS()
class AHexTile : public AActor
{
	GENERATED_BODY()

	UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* HexMesh;

	
public:	
	AHexTile();
	bool isActive;
	UPROPERTY()
	class UMaterial* BaseMaterial;

	UPROPERTY()
	class UMaterialInstance* BlueMaterial;
	
	UPROPERTY()
	class AAutoChessGrid* HexGrid;

	UFUNCTION()
	void Hover(bool bOn);

	UFUNCTION()
		void ChangeObjectChannel();
	UPROPERTY(VisibleAnywhere)
	FVector2D coords;
	UPROPERTY(VisibleAnywhere)
	bool isOccupied;
	virtual void BeginPlay() override;

	

public:
	class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	class UStaticMeshComponent* GetHexMesh() const { return HexMesh; }
	int32 x_index;
	int32 y_index;
	
	//FVector4 CoordsLocation;
};
