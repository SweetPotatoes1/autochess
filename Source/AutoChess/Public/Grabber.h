// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AUTOCHESS_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	TEnumAsByte<EObjectTypeQuery> ObjectToTrace;
	FHitResult hit, hit2;
	bool wasReleased;
	
	

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		FHitResult Trace();

	UFUNCTION()
		FHitResult TraceObject();
	UFUNCTION()
		void Grab();
	UFUNCTION()
		void Release();

private:

	class UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;

	
};
