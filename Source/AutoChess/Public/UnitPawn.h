// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "AutoChessGrid.h"
#include "SmallPlatform.h"
#include "UnitPawn.generated.h"

class AGameLogicManager;
UCLASS()
class AUTOCHESS_API AUnitPawn : public APawn
{
	GENERATED_BODY()
		UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	UPROPERTY(Category = Hex, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* UnitMesh;
public:
	// Sets default values for this pawn's properties
	AUnitPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AGameLogicManager* manager;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AHexTile* CurrentHex;
		AHexTile* TargetHex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		ASmallPlatform* CurrentPlatform;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FVector2D TargetCoords;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AUnitPawn* TargetUnit;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AAutoChessGrid* MyGrid;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<AHexTile*> NeighbourHexs;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<AUnitPawn*> EnemyUnits;
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool friendly;
	UFUNCTION()
		void SetCurrentHex(AHexTile* hex);
	UFUNCTION()
		void MoveToHex(AHexTile* hex);
	UFUNCTION()
		void SetCurrentPlatform(ASmallPlatform* platform);
	UFUNCTION()
		void MoveToPlatform(ASmallPlatform* platform);
	UFUNCTION()
		void MoveToEnemy(AUnitPawn* pawn);
	UFUNCTION()
	TArray<AHexTile*> GetNeighbours();

	void MoveUp();
	void MoveDown();
	void MoveRightUp();
	void MoveRightDown();
	void MoveLeftUp();
	void MoveLeftDown();
	void Attack(AUnitPawn* ThisPawn, AUnitPawn* EnemyPawn);
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool isActive;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool moved;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int CurrentHealth;

	bool bCanMove;
	bool bCanAttack;
	FTimerHandle MoveDelay;
	FTimerHandle AttackDelay;
	void ResetMove();
	void ResetAttackTimer();
	float TurnTimer;
	float AttackSpeed;
	int MaxHealth;
	
	int AD;
	
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	class UStaticMeshComponent* GetHexMesh() const { return UnitMesh; }
	
	
};
