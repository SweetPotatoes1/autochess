// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HexTile.h"
#include "UnitPawn.h"
#include "GameFramework/Actor.h"
#include "GameLogicManager.generated.h"

UCLASS()
class AUTOCHESS_API AGameLogicManager : public AActor
{
	GENERATED_BODY()
		UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* DummyRoot;

	
public:	
	// Sets default values for this actor's properties
	AGameLogicManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AUnitPawn*> Units;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<AHexTile*> Tiles;
	class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	AUnitPawn* FindClosestEnemy(TArray<AUnitPawn*> pawns, AUnitPawn* pawn);
};
