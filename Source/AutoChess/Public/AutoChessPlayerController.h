// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AutoChessPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AUTOCHESS_API AAutoChessPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AAutoChessPlayerController();

};
